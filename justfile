# run tests with a single thread
test args="":
    RUST_LOG=info cargo test {{args}} -- --test-threads=1
