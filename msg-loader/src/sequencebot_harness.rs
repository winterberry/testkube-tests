//! A test harness for tests where MsgLoader is set up to receive messages from SequenceBot.

use std::time::Duration;

use reqwest::Method;
use tokio::time::sleep;
use tracing::info;
use twitch_api::helix::users::User;

use crate::{api, elasticsearch, twitch};

pub struct SequenceBotHarness {
    pub specific_user: User,
}

#[must_use]
pub async fn setup_msg_loader_with_sequencebot() -> SequenceBotHarness {
    // identify the UID of the specific twitch stream
    let specific_user = twitch::get_specific_user("hello_abc_f4ec").await;
    info!("Identified UID of the specific user: {:?}", specific_user);

    // Remove user from the system before the test
    api::delete_user(&specific_user.id).await;

    // wait a bit to make sure user deletion is registered
    sleep(Duration::from_secs(5)).await;

    // clear all messages that we might have of this user
    let elasticsearch_client = elasticsearch::connect_to_elasticsearch().await;
    info!("Connected to Elasticsearch client",);
    elasticsearch::clear_by_userid(&elasticsearch_client, &specific_user.id).await;
    info!("Cleared messages for user: {}", specific_user.id);

    // ensure that there are currently 0 messages for this user
    let message_count = api::get_msg_count_through_wb_api(&specific_user.id).await;
    info!(
        "Current message count for user {}: {message_count}",
        specific_user.id
    );
    assert_eq!(message_count, 0);

    // add this user to the system
    // POST request new member to the Project Winterberry REST API
    let response = api::api_request(
        Method::POST,
        "user",
        "",
        serde_json::json!({
            "twitch_uid": specific_user.id,
            "channel_name": format!("#{}", specific_user.login),
        }),
    )
    .await;

    let response_status = response.status();

    assert!(response_status.is_success());
    info!("User added successfully");

    // wait a bit to make sure that messages are recorded into the system
    // also, it takes about 10 seconds for the user registration to be processed
    info!("Waiting for 60 seconds to allow messages to be recorded");
    sleep(Duration::from_secs(60)).await;
    info!("Finished waiting");

    SequenceBotHarness { specific_user }
}

impl Drop for SequenceBotHarness {
    fn drop(&mut self) {
        // Remove user from the system after the test
        tokio::runtime::Handle::current().block_on(async {
            api::delete_user(&self.specific_user.id).await;
        });
    }
}
