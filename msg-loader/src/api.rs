use kube_discovery::kube::{Client, Config};
use kube_discovery::LabelSelector;
use reqwest::header::{HeaderMap, CONTENT_TYPE};
use reqwest::Method;
use tracing::info;
use tracing::{debug, error};
use twitch_api::types::UserId;

use crate::{API_GATEWAY_LABELS, API_PATH};

pub async fn get_msg_count_through_wb_api(user_id: &twitch_api::types::UserId) -> usize {
    let response = api_request(
        Method::GET,
        "message_count",
        &format!("?twitch_uid={user_id}"),
        "".into(),
    )
    .await;

    if !response.status().is_success() {
        error!("Failed to get message count. HTTP Response: {response:?}");
        panic!("Failed to get message count for user, HTTP Response: {response:?}",);
    }

    let response_body = response.text().await.unwrap();
    info!("/message_count Response body: {response_body}");
    serde_json::from_str::<serde_json::Value>(&response_body).unwrap()["count"]
        .as_u64()
        .unwrap() as usize
}

pub async fn get_messages_through_wb_api(
    user_id: &twitch_api::types::UserId,
) -> Vec<serde_json::Value> {
    let response = api_request(
        Method::GET,
        "message",
        &format!("?twitch_uid={user_id}"),
        "".into(),
    )
    .await;

    if !response.status().is_success() {
        error!("Failed to get messages. HTTP Response: {response:?}");
        panic!("Failed to get messages for user, HTTP Response: {response:?}",);
    }

    let response_body = response.text().await.unwrap();
    debug!("Response body: {response_body}");
    serde_json::from_str::<serde_json::Value>(&response_body).unwrap()["messages"]
        .as_array()
        .unwrap()
        .clone()
}

pub async fn delete_user(user_id: &UserId) {
    api_request(
        Method::DELETE,
        &format!("user/{}", user_id),
        "",
        serde_json::json!({}),
    )
    .await;
}

/// Get the path to the Project Winterberry public API
pub async fn get_api_path(endpoint: &str) -> String {
    let kube_config = Config::infer().await.unwrap();
    let kube_client = Client::try_from(kube_config.clone()).unwrap();

    let api_address = LabelSelector(API_GATEWAY_LABELS)
        .load_service_host(&kube_config, &kube_client)
        .await
        .unwrap();
    format!("http://{api_address}/{API_PATH}/{endpoint}")
}

/// Send a REST request to the Project Winterberry public API message endpoint
pub async fn api_request(
    method: reqwest::Method,
    endpoint: &str,
    resource_path: &str,
    data: serde_json::Value,
) -> reqwest::Response {
    let api_path = format!("{}{resource_path}", get_api_path(endpoint).await);
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, "application/json".parse().unwrap());

    info!("Sending {method:?} request to REST API endpoint at {api_path}. Data: {data:#?}",);
    let client = reqwest::Client::new();
    client
        .request(method, &api_path)
        .headers(headers)
        .body(data.to_string())
        .send()
        .await
        .unwrap()
}
