use elasticsearch::auth::Credentials;
use elasticsearch::cert::CertificateValidation;
use elasticsearch::http::transport::{SingleNodeConnectionPool, TransportBuilder};
use elasticsearch::Elasticsearch;
use kube_discovery::kube::{Client, Config};
use kube_discovery::LabelSelector;
use reqwest::{StatusCode, Url};
use secrecy::ExposeSecret;
use tracing::info;
use twitch_api::types::UserId;

const ELASTICSEARCH_LABELS: &str = "app=chatmsg-elasticsearch,environment=staging";
const MONITOR_LABELS: &str = "app=weechat-monitor,environment=staging";

pub async fn connect_to_elasticsearch() -> Elasticsearch {
    let kube_config = Config::infer().await.unwrap();
    let kube_client = Client::try_from(kube_config.clone()).unwrap();

    let elasticsearch_url = {
        let host = LabelSelector(ELASTICSEARCH_LABELS)
            .load_service_host(&kube_config, &kube_client)
            .await
            .unwrap();
        Url::parse(&format!("https://{host}")).unwrap()
    };

    let elasticsearch_credentials = {
        let user = "elastic";
        let password = LabelSelector(MONITOR_LABELS)
            .load_secret_value_through_workload(&kube_client, "ELASTICSEARCH_PASSWORD")
            .await
            .unwrap()
            .expose_secret()
            .clone();
        Credentials::Basic(user.into(), password.clone())
    };

    let connection_pool = SingleNodeConnectionPool::new(elasticsearch_url);
    let transport = TransportBuilder::new(connection_pool)
        .auth(elasticsearch_credentials)
        .cert_validation(CertificateValidation::None)
        .build()
        .unwrap();
    Elasticsearch::new(transport)
}

pub async fn clear_by_userid(elasticsearch_client: &Elasticsearch, user_id: &UserId) {
    let delete_response = elasticsearch_client
        .delete_by_query(elasticsearch::DeleteByQueryParts::Index(&["irc-messages"]))
        .body(serde_json::json!({
            "query": {
                "match": {
                    "twitch_uid": user_id
                }
            }
        }))
        .refresh(true)
        .send()
        .await
        .unwrap();

    if delete_response.status_code() == StatusCode::OK {
        info!("Successfully deleted messages for user {user_id}");
    } else {
        panic!("Failed to delete messages for user {user_id}, HTTP Response: {delete_response:?}");
    }
}
