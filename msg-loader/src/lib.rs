pub(crate) const API_PATH: &str = "v1staging";
pub(crate) const API_GATEWAY_LABELS: &str = "app=gateway-svc-tyk-oss-tyk-gateway";

pub mod api;
pub mod elasticsearch;
pub mod sequencebot_harness;
pub mod twitch;
