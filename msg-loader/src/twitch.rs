
use kube_discovery::kube::{Client, Config};
use kube_discovery::LabelSelector;
use secrecy::ExposeSecret;
use tracing::info;
use twitch_api::helix::streams::{GetStreamsRequest, Stream};
use twitch_api::helix::users::User;
use twitch_api::twitch_oauth2::{AppAccessToken, ClientId, ClientSecret};
use twitch_api::TwitchClient;

pub const CHANNEL_SYNC_KUBE_LABELS: &str =
    "app=sync-tracked-channels-with-registered-users,environment=staging";
pub const CLIENT_ID_SECRET_NAME: &str = "TWITCH_CLIENT_ID";
pub const CLIENT_SECRET_SECRET_NAME: &str = "TWITCH_CLIENT_SECRET";

pub async fn load_twitch_client_id() -> ClientId {
    let secret_value = {
        let kube_config = Config::infer().await.unwrap();
        let kube_client = Client::try_from(kube_config.clone()).unwrap();
        LabelSelector(CHANNEL_SYNC_KUBE_LABELS)
            .load_secret_value_through_workload(&kube_client, CLIENT_ID_SECRET_NAME)
            .await
            .unwrap()
    };

    ClientId::from(secret_value.expose_secret().clone())
}

pub async fn load_twitch_client_secret() -> ClientSecret {
    let secret_value = {
        let kube_config = Config::infer().await.unwrap();
        let kube_client = Client::try_from(kube_config.clone()).unwrap();
        LabelSelector(CHANNEL_SYNC_KUBE_LABELS)
            .load_secret_value_through_workload(&kube_client, CLIENT_SECRET_SECRET_NAME)
            .await
            .unwrap()
    };

    ClientSecret::from(secret_value.expose_secret().clone())
}

pub async fn get_biggest_live_streams(num_streams: usize) -> Vec<Stream> {
    let client = TwitchClient::<'static, reqwest::Client>::new();

    info!("Requesting AppAccessToken from Twitch API");

    let client_id = load_twitch_client_id().await;
    let client_secret = load_twitch_client_secret().await;

    info!("Twitch client_id: {client_id}");

    let token = AppAccessToken::get_app_access_token(&client, client_id, client_secret, vec![])
        .await
        .unwrap();

    info!("Getting the biggest live stream");

    let request = GetStreamsRequest::builder().first(num_streams).build();
    let biggest_live_streams = client
        .helix
        .req_get(request, &token)
        .await
        .unwrap()
        .data
        .into_iter()
        .take(num_streams)
        .collect();

    biggest_live_streams
}

pub async fn get_specific_user(user_login: impl AsRef<str>) -> User {
    let client = TwitchClient::<'static, reqwest::Client>::new();

    info!("Requesting AppAccessToken from Twitch API");

    let client_id = load_twitch_client_id().await;
    let client_secret = load_twitch_client_secret().await;

    info!("Twitch client_id: {client_id}");

    let token = AppAccessToken::get_app_access_token(&client, client_id, client_secret, vec![])
        .await
        .unwrap();

    let user_login = user_login.as_ref();
    info!("Getting the specific user for login {user_login}");

    let specific_user = client
        .helix
        .get_user_from_login(user_login, &token)
        .await
        .unwrap()
        .unwrap();

    specific_user
}
