use msg_loader::sequencebot_harness::setup_msg_loader_with_sequencebot;
use tracing::info;

#[test_log::test(tokio::test)]
async fn message_count() {
    let sequencebot_harness = setup_msg_loader_with_sequencebot().await;

    let specific_user = &sequencebot_harness.specific_user;
    let message_count = msg_loader::api::get_msg_count_through_wb_api(&specific_user.id).await;
    info!(
        "Current message count for user {}: {message_count}",
        specific_user.id
    );

    assert!(message_count > 50);
}
