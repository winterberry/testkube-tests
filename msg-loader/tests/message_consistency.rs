use std::str::FromStr;

use msg_loader::api;
use msg_loader::sequencebot_harness::setup_msg_loader_with_sequencebot;
use tracing::info;

/// This test ensures that no messages are lost while the system is running.
#[test_log::test(tokio::test)]
async fn message_consistency() -> Result<(), Box<dyn std::error::Error>> {
    // Start MsgLoader and set it up to receive messages sent by SequenceBot
    let sequencebot_harness = setup_msg_loader_with_sequencebot().await;

    let specific_user = &sequencebot_harness.specific_user;
    let messages = api::get_messages_through_wb_api(&specific_user.id).await;
    info!("New messages for user {}: {messages:#?}", specific_user.id);

    let mut message_count_ids = messages
        .iter()
        .filter_map(|msg| msg["message"].as_str())
        .filter_map(parse_irc_str_as_privmsg)
        .map(|(_msg_sender, msg_text)| msg_text)
        .filter_map(get_count_id_from_sequencebot_message)
        .collect::<Vec<_>>();
    message_count_ids.sort();

    info!("Extracted counts: {message_count_ids:?}");

    // Ensure that User is removed before the test might fail
    std::mem::drop(sequencebot_harness);

    // Check for missing IDs
    let missing_ids = get_missing_ids(&message_count_ids);
    assert!(
        missing_ids.is_empty(),
        "Missing Sequence IDs: {missing_ids:?}"
    );
    Ok(())
}

/// Function to get missing IDs from a vector of subsequent IDs.
fn get_missing_ids(ids: &Vec<i64>) -> Vec<i64> {
    if let (Some(&min_id), Some(&max_id)) = (ids.iter().min(), ids.iter().max()) {
        (min_id..=max_id).filter(|id| !ids.contains(id)).collect()
    } else {
        vec![]
    }
}

/// Parses a string that is an IRC message into a tuple of the sender and the message text.
fn parse_irc_str_as_privmsg<T: AsRef<str>>(message_str: T) -> Option<(String, String)> {
    if let Ok(parsed_message) = irc_proto::Message::from_str(message_str.as_ref()) {
        if let irc_proto::Command::PRIVMSG(param1, param2) = parsed_message.command {
            return Some((param1.to_string(), param2.to_string()));
        }
    }
    None
}

/// Extracts the count ID from a SequenceBot message.
fn get_count_id_from_sequencebot_message<T: AsRef<str>>(msg_text: T) -> Option<i64> {
    if let Ok(json_value) = serde_json::from_str::<serde_json::Value>(msg_text.as_ref()) {
        return json_value["count"].as_i64();
    }
    None
}
