use std::time::Duration;

use msg_loader::{api, elasticsearch, twitch};
use reqwest::Method;
use tokio::time::sleep;
use tracing::info;

#[test_log::test(tokio::test)]
async fn retrieve_messages() -> Result<(), Box<dyn std::error::Error>> {
    // identify the UID of the currently largest twitch stream
    let biggest_live_streams = twitch::get_biggest_live_streams(10).await;
    info!(
        "Identified UIDs of the biggest live streams: {:?}",
        biggest_live_streams
            .iter()
            .map(|s| (&s.user_id, &s.user_login, &s.viewer_count))
            .collect::<Vec<_>>()
    );

    // Remove users from the system before the test
    for live_stream in &biggest_live_streams {
        api::delete_user(&live_stream.user_id).await;
    }

    // wait a bit to make sure user deletions are registered
    sleep(Duration::from_secs(20)).await;

    // clear all messages that we might have of this user
    for live_stream in &biggest_live_streams {
        let elasticsearch_client = elasticsearch::connect_to_elasticsearch().await;
        info!("Connected to Elasticsearch client",);
        elasticsearch::clear_by_userid(&elasticsearch_client, &live_stream.user_id).await;
        info!("Cleared messages for user: {}", live_stream.user_id);
    }

    // ensure that there are currently 0 messages for this user
    for live_stream in &biggest_live_streams {
        let message_count = api::get_msg_count_through_wb_api(&live_stream.user_id).await;
        info!(
            "Current message count for user {}: {message_count}",
            live_stream.user_id
        );
        assert_eq!(message_count, 0);
    }

    // add this user to the system
    // POST request new member to the Project Winterberry REST API
    for live_stream in &biggest_live_streams {
        let response = api::api_request(
            Method::POST,
            "user",
            "",
            serde_json::json!({
                "twitch_uid": live_stream.user_id,
                "channel_name": format!("#{}",live_stream.user_login),
            }),
        )
        .await;

        let response_status = response.status();

        assert!(response_status.is_success());
        info!("User added successfully");
    }

    // wait a bit to make sure that messages are recorded into the system
    // also, it takes about 10 seconds for the user registration to be processed
    info!("Waiting for 60 seconds to allow messages to be recorded");
    sleep(Duration::from_secs(60)).await;
    info!("Finished waiting");

    let mut total_message_count = 0;
    for live_stream in &biggest_live_streams {
        let message_count = api::get_msg_count_through_wb_api(&live_stream.user_id).await;
        info!(
            "New message count for user {}: {message_count}",
            live_stream.user_id
        );
        total_message_count += message_count;
    }

    // Remove users from the system after the test
    for live_stream in &biggest_live_streams {
        api::delete_user(&live_stream.user_id).await;
    }

    assert!(total_message_count >= 100);
    Ok(())
}
