// we're just importing url to make sure imports are working
use url as _;

#[test]
fn pass_always() {
    assert_eq!(1 + 1, 2);
}
