use redis::Commands;
use reqwest::Method;
use serde_json::json;
use std::time::Duration;
use tokio::time::sleep;
use tracing::info;
use user_manager::{
    api_request, connect_to_redis, get_registered_user_key, NEW_MEMBER_CHANNEL_NAME,
    NEW_MEMBER_POST_DATA, NEW_MEMBER_TWITCH_UID,
};

/// In this test, we add a new member to our system and then retrieve the member using the GET endpoint.
#[test_log::test(tokio::test)]
async fn get_member() -> Result<(), Box<dyn std::error::Error>> {
    let mut redis_connection = connect_to_redis().await;

    let registered_users_key = get_registered_user_key().await;

    let _: () = redis_connection.del(&registered_users_key)?;
    info!("Deleted registered users data from Redis");

    // POST request new member to the Project Winterberry REST API
    let response = api_request(Method::POST, "", NEW_MEMBER_POST_DATA.clone()).await;
    info!("Response: {response:?}");

    assert!(response.status().is_success());

    // ensure that new test member is registered in Redis
    let start_time = tokio::time::Instant::now();
    let mut user_is_registered = false;

    while start_time.elapsed() < Duration::from_secs(5) {
        user_is_registered =
            redis_connection.sismember(&registered_users_key, NEW_MEMBER_TWITCH_UID)?;
        if user_is_registered {
            break;
        }
        sleep(Duration::from_millis(250)).await;
    }

    assert!(
        user_is_registered,
        "User was not registered in Redis within 5 seconds"
    );

    // GET request to retrieve the member
    let escaped_channel_name = urlencoding::encode(NEW_MEMBER_CHANNEL_NAME);
    let response = api_request(
        Method::GET,
        &format!("/?twitch_username={escaped_channel_name}&twitch_uid={NEW_MEMBER_TWITCH_UID}"),
        json!({}),
    )
    .await;
    info!("Response: {response:?}");

    assert_eq!(response.status(), reqwest::StatusCode::OK);

    // GET request to retrieve a non-existent member
    let non_existent_uid = "100000000000";
    let non_existent_channelname = urlencoding::encode("#non_existent_username");
    let response = api_request(
        Method::GET,
        &format!("/?twitch_username={non_existent_channelname}&twitch_uid={non_existent_uid}"),
        json!({}),
    )
    .await;
    info!("Response for non-existent user: {response:?}");

    assert_eq!(response.status(), reqwest::StatusCode::NO_CONTENT);

    Ok(())
}
