use redis::Commands;
use reqwest::Method;
use serde_json::json;
use std::time::Duration;
use tokio::time::sleep;
use tracing::info;
use user_manager::{
    api_request, connect_to_redis, get_registered_user_key, get_tracked_channels_key,
    NEW_MEMBER_CHANNEL_NAME, NEW_MEMBER_POST_DATA, NEW_MEMBER_TWITCH_UID,
};

/// In this test, we remove a member from our system and check that it's not contained in the registered users database anymore
#[test_log::test(tokio::test)]
async fn delete_member() -> Result<(), Box<dyn std::error::Error>> {
    let mut redis_connection = connect_to_redis().await;

    let tracked_channels_key = get_tracked_channels_key().await;
    let registered_users_key = get_registered_user_key().await;

    info!("Key for tracked channels data: {tracked_channels_key}");
    info!("Key for registered users data: {registered_users_key}");

    let _: () = redis_connection.del(&tracked_channels_key)?;
    info!("Deleted tracked channels data from Redis");

    let _: () = redis_connection.del(&registered_users_key)?;
    info!("Deleted registered users data from Redis");

    // POST request new member to the Project Winterberry REST API
    let response = api_request(Method::POST, "", NEW_MEMBER_POST_DATA.clone()).await;
    info!("Response: {response:?}");

    sleep(Duration::from_millis(2000)).await;
    let channel_is_tracked: bool =
        redis_connection.sismember(&tracked_channels_key, NEW_MEMBER_CHANNEL_NAME)?;
    assert!(channel_is_tracked);

    // DELETE request new member
    let response = api_request(
        Method::DELETE,
        &format!("/{NEW_MEMBER_TWITCH_UID}"),
        json!({}),
    )
    .await;
    assert!(response.status().is_success());

    // ensure that user is not registered anymore
    let user_is_registered: bool =
        redis_connection.sismember(&registered_users_key, NEW_MEMBER_TWITCH_UID)?;
    assert!(!user_is_registered);

    Ok(())
}

/// This test does the reverse of `post_member_and_track`. It deletes a member from the system and checks if the member's channel is removed from the list of tracked channels.
#[test_log::test(tokio::test)]
async fn delete_member_and_untrack() -> Result<(), Box<dyn std::error::Error>> {
    let mut redis_connection = connect_to_redis().await;

    let tracked_channels_key = get_tracked_channels_key().await;
    let registered_users_key = get_registered_user_key().await;

    info!("Key for tracked channels data: {tracked_channels_key}");
    info!("Key for registered users data: {registered_users_key}");

    let _: () = redis_connection.del(&tracked_channels_key)?;
    info!("Deleted tracked channels data from Redis");

    let _: () = redis_connection.del(&registered_users_key)?;
    info!("Deleted registered users data from Redis");

    // POST request new member to the Project Winterberry REST API
    let response = api_request(Method::POST, "", NEW_MEMBER_POST_DATA.clone()).await;
    info!("Response: {response:?}");

    sleep(Duration::from_millis(2000)).await;
    let channel_is_tracked: bool =
        redis_connection.sismember(&tracked_channels_key, NEW_MEMBER_CHANNEL_NAME)?;
    assert!(channel_is_tracked);

    // DELETE request new member
    let response = api_request(
        Method::DELETE,
        &format!("/{NEW_MEMBER_TWITCH_UID}"),
        json!({}),
    )
    .await;
    assert!(response.status().is_success());

    // ensure that channel is not tracked anymore
    sleep(Duration::from_millis(2000)).await;
    let channel_is_tracked: bool =
        redis_connection.sismember(&tracked_channels_key, NEW_MEMBER_CHANNEL_NAME)?;
    assert!(!channel_is_tracked);

    Ok(())
}
