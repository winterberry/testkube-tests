use redis::Commands;
use reqwest::Method;
use std::time::Duration;
use tokio::time::sleep;
use tracing::info;
use user_manager::{
    api_request, connect_to_redis, get_registered_user_key, get_tracked_channels_key,
    NEW_MEMBER_CHANNEL_NAME, NEW_MEMBER_POST_DATA, NEW_MEMBER_TWITCH_UID,
};

/// In this test, we add a new member to our system and check if they can be found in the dataset of registered users.
#[test_log::test(tokio::test)]
async fn post_member() -> Result<(), Box<dyn std::error::Error>> {
    let mut redis_connection = connect_to_redis().await;

    let registered_users_key = get_registered_user_key().await;

    let _: () = redis_connection.del(&registered_users_key)?;
    info!("Deleted registered users data from Redis");

    // POST request new member to the Project Winterberry REST API
    let response = api_request(Method::POST, "", NEW_MEMBER_POST_DATA.clone()).await;
    info!("Response: {response:?}");

    assert!(response.status().is_success());

    // check if user is now stored in Redis
    let user_is_registered: bool =
        redis_connection.sismember(&registered_users_key, NEW_MEMBER_TWITCH_UID)?;

    assert!(user_is_registered);

    Ok(())
}

/// In this test, we add a new member to our system, thorugh Project Winterberry's REST API. We then check if the member's channel is in the list of tracked channels.
/// This tests whether the users added through `user-manager` are added to the tracked channels as expected.
#[test_log::test(tokio::test)]
async fn post_member_and_track() -> Result<(), Box<dyn std::error::Error>> {
    let mut redis_connection = connect_to_redis().await;

    let tracked_channels_key = get_tracked_channels_key().await;
    let registered_users_key = get_registered_user_key().await;

    info!("Key for tracked channels data: {tracked_channels_key}");
    info!("Key for registered users data: {registered_users_key}");

    let _: () = redis_connection.del(&tracked_channels_key)?;
    info!("Deleted tracked channels data from Redis");

    let _: () = redis_connection.del(&registered_users_key)?;
    info!("Deleted registered users data from Redis");

    // POST request new member to the Project Winterberry REST API
    let response = api_request(Method::POST, "", NEW_MEMBER_POST_DATA.clone()).await;
    info!("Response: {response:?}");

    assert!(response.status().is_success());

    sleep(Duration::from_millis(2000)).await;

    // check if member's channel is in list of tracked channels
    let channel_is_tracked: bool =
        redis_connection.sismember(&tracked_channels_key, NEW_MEMBER_CHANNEL_NAME)?;

    assert!(channel_is_tracked);

    // cleanup
    let _: () = redis_connection.del(&tracked_channels_key)?;

    Ok(())
}
