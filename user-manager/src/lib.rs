use kube_discovery::k8s_openapi::api::apps::v1::Deployment;
use kube_discovery::k8s_openapi::api::core::v1::ConfigMap;
use kube_discovery::kube::{Client, Config};
use kube_discovery::{LabelSelector, Workload};
use once_cell::sync::Lazy;
use reqwest::header::{HeaderMap, CONTENT_TYPE};
use secrecy::ExposeSecret;
use tracing::info;

const REDIS_LABELS: &str = "app=redis-twitch-observer,environment=staging";
const REDIS_PASSWORD_ENV_VAR: &str = "REDIS_PASSWORD";
const CONTROLLER_CONFIG_LABELS: &str = "app=weechat-controller,config=config,environment=staging";
const USER_MANAGER_APP_LABELS: &str = "app=user-manager,environment=staging";
const API_PATH: &str = "v1staging/user";
const API_GATEWAY_LABELS: &str = "app=gateway-svc-tyk-oss-tyk-gateway";

pub const NEW_MEMBER_CHANNEL_NAME: &str = "#twitch";
pub const NEW_MEMBER_TWITCH_UID: &str = "12826";
pub static NEW_MEMBER_POST_DATA: Lazy<serde_json::Value> = Lazy::new(
    || serde_json::json!({"twitch_uid": NEW_MEMBER_TWITCH_UID, "channel_name": NEW_MEMBER_CHANNEL_NAME}),
);

pub async fn connect_to_redis() -> redis::Connection {
    let kube_config = Config::infer().await.unwrap();
    let kube_client = Client::try_from(kube_config.clone()).unwrap();

    let redis_url = {
        let redis_host = LabelSelector(REDIS_LABELS)
            .load_service_host(&kube_config, &kube_client)
            .await
            .unwrap();

        let redis_password = LabelSelector(REDIS_LABELS)
            .load_secret_value_through_workload(&kube_client, REDIS_PASSWORD_ENV_VAR)
            .await
            .unwrap()
            .expose_secret()
            .clone();
        let redis_password_url = urlencoding::encode(&redis_password);

        info!("Connecting to redis on {redis_host}");
        format!("redis://:{redis_password_url}@{redis_host}")
    };

    redis::Client::open(redis_url)
        .unwrap()
        .get_connection()
        .unwrap()
}

pub async fn get_tracked_channels_key() -> String {
    let kube_config = Config::infer().await.unwrap();
    let kube_client = Client::try_from(kube_config.clone()).unwrap();
    let controller_config_map: ConfigMap = LabelSelector(CONTROLLER_CONFIG_LABELS)
        .find_resource(&kube_client)
        .await
        .unwrap();
    controller_config_map
        .data
        .unwrap()
        .get("REDIS_SET_NAME")
        .unwrap()
        .clone()
}

pub async fn get_registered_user_key() -> String {
    let kube_config = Config::infer().await.unwrap();
    let kube_client = Client::try_from(kube_config.clone()).unwrap();
    let user_manager_deployment: Deployment = LabelSelector(USER_MANAGER_APP_LABELS)
        .find_resource(&kube_client)
        .await
        .unwrap();

    user_manager_deployment
        .find_env_var("REDIS_SET_NAME")
        .unwrap()
        .value
        .unwrap()
}

/// Get the path to the Project Winterberry public API
pub async fn get_api_path() -> String {
    let kube_config = Config::infer().await.unwrap();
    let kube_client = Client::try_from(kube_config.clone()).unwrap();

    let api_address = LabelSelector(API_GATEWAY_LABELS)
        .load_service_host(&kube_config, &kube_client)
        .await
        .unwrap();
    format!("http://{api_address}/{API_PATH}")
}

/// Send a REST request to the Project Winterberry public API
pub async fn api_request(
    method: reqwest::Method,
    resource_path: &str,
    data: serde_json::Value,
) -> reqwest::Response {
    let api_path = format!("{}{resource_path}", get_api_path().await);
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, "application/json".parse().unwrap());

    info!("Sending {method:?} request to REST API endpoint at {api_path}. Data: {data:#?}",);
    let client = reqwest::Client::new();
    client
        .request(method, &api_path)
        .headers(headers)
        .body(data.to_string())
        .send()
        .await
        .unwrap()
}
